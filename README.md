## technilcal task
We make a site for a small, provincial theater. The city N, where the theater is small, does not have a lot of public, so the performances are held strictly once a day, at 19:00.
First of all, we have to implement a poster. We have a theater administrator, Oksana Grigoryevna, a respected woman. She will make a schedule of performances.
The performance is characterized by the following properties:

- Name
- Start date (02/19/2019)
- End date (03/21/2019)

We have create, index, delete methods.
When adding a new performance, if we already have a performance for these dates - we display an error.
You do not need to write an authorization system, we just close access to the create \ delete methods using nginx in the future.
Web interface only with great desire, enough API methods.

Tests are also very welcome.
As a DB, I recommend looking at Postgresql and non-standard data types.

## setup
1. Download source code: git clone git@bitbucket.org:vladr/events_test.git
2. Run: bundle install
3. Run: bundle exec rake db:setup
4. Run: bundle exec rake db:seed
5. Run: bundle exec rackup
6. Open in browser: http://localhost:9292/events

## add event
curl -X POST  http://localhost:9292/events -H "Content-Type: application/json" -d "{\"event\":{\"title\":\"Ruby Conf 2020\",\"start_date\":\"2020-01-01\",\"end_date\":\"2020-01-30\"}}"

## delete event
curl -X "DELETE" http://localhost:9292/events/:id

## Tests
bundle exec rspec


