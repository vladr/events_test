require File.expand_path '../../spec_helper.rb', __FILE__

RSpec.describe 'API - Events', type: :request do
  describe '#index' do
    let(:url) { '/events' }
    let(:start_date) { Date.today.to_s.freeze }
    let(:end_date) { 1.day.since.to_date.to_s.freeze }
    let!(:event) { Event.create!(title: 'Ruby Conf', start_date: start_date, end_date: end_date) }

    it 'renders events json' do
      get url

      res = JSON.parse(last_response.body)
      expect(last_response.status).to eq(200)
      expect(res[0]['title']).to eq('Ruby Conf')
      expect(res[0]['duration']).to eq((start_date...(Date.parse(end_date) + 1.day).to_s).to_s)
    end
  end

  describe  '#create' do
    let(:url) { '/events' }
    let(:start_date) { Date.today.to_s.freeze }
    let(:end_date) { 1.day.since.to_date.to_s.freeze }

    it 'creates event' do
      expect {
        post url, { event: {title: 'Ruby Conf', start_date: start_date, end_date: end_date }}.to_json
      }.to change(Event, :count)

      res = JSON.parse(last_response.body)
      expect(last_response.status).to eq(201)
      expect(res['title']).to eq('Ruby Conf')
    end

    it 'render errors' do
      expect {
        post url, {event: {}}.to_json
      }.to_not change(Event, :count)

      res = JSON.parse(last_response.body)
      expect(last_response.status).to eq(422)
      expect(res['errors']).to include("duration" => ["can't be blank"])
      expect(res['errors']).to include("end_date" => ["can't be blank"])
      expect(res['errors']).to include("start_date" => ["can't be blank"])
      expect(res['errors']).to include("title" => ["can't be blank"])
    end
  end

  describe '#delete' do
    let(:url) { '/events' }
    let(:start_date) { Date.today.to_s.freeze }
    let(:end_date) { 1.day.since.to_date.to_s.freeze }
    let!(:event) { Event.create!(title: 'Ruby Conf', start_date: start_date, end_date: end_date) }

    it 'deletes event' do
      expect {
        delete url + "/#{event.id}"
      }.to change(Event, :count).by(-1)

      expect(last_response.status).to eq(200)
    end
  end
end
