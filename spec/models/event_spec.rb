require File.expand_path '../../spec_helper.rb', __FILE__

describe Event do
  describe 'validations' do
    let(:event) { Event.new(title: 'Ruby Conf', start_date: Date.today.to_s, end_date: 1.week.since.to_date.to_s) }
    let(:new_event) { Event.create(title: 'Ruby Conf 2', start_date: 5.days.since.to_date.to_s, end_date: 10.days.since.to_date.to_s) }

    it 'creates valid event' do
      expect {
        event.save
      }.to change(Event, :count).by(1)
    end

    it 'invalid if title missing' do
      event.title = nil
      expect {
        event.save
      }.to_not change(Event, :count)
      expect(event.errors.count).to eq(1)
      expect(event.errors[:title]).to include("can't be blank")
    end

    it 'invalid if start_date missing' do
      event.start_date = nil
      expect {
        event.save
      }.to_not change(Event, :count)
      expect(event.errors.count).to eq(2)
      expect(event.errors[:start_date]).to include("can't be blank")
      expect(event.errors[:duration]).to include("can't be blank")
    end

    it 'invalid if end_date missing' do
      event.end_date = nil
      expect {
        event.save
      }.to_not change(Event, :count)
      expect(event.errors.count).to eq(2)
      expect(event.errors[:end_date]).to include("can't be blank")
      expect(event.errors[:duration]).to include("can't be blank")
    end

    it 'invalid if duration set but missed start and end dates' do
      event.start_date = nil
      event.end_date = nil
      event.duration = Date.today..Date.today
      expect {
        event.save
      }.to_not change(Event, :count)
      expect(event.errors.count).to eq(2)
      expect(event.errors[:start_date]).to include("can't be blank")
      expect(event.errors[:end_date]).to include("can't be blank")
    end

    it 'renders error if duration overlapped' do
      event.save
      expect {
        new_event.save
      }.to_not change(Event, :count)
      expect(new_event.errors.count).to eq(1)
      expect(new_event.errors[:duration]).to include("already exists")
    end
  end

  describe 'duration' do
    let(:ruby_event) { Event.new(title: 'Ruby Conf', start_date: '2019-12-01', end_date: '2019-12-10') }
    let(:elixir_event) { Event.new(title: 'Elixir Conf', start_date: '2019-12-11', end_date: '2019-12-20') }
    let(:golang_event) { Event.new(title: 'Golang Conf', start_date: '2019-12-21', end_date: '2019-12-30') }

    it 'allows to create events' do
      expect {
        ruby_event.save
        elixir_event.save
        golang_event.save
      }.to change(Event, :count).by(3)
      expect(ruby_event.duration).to eq(Date.parse('2019-12-01')..Date.parse('2019-12-10'))
      expect(elixir_event.duration).to eq(Date.parse('2019-12-11')..Date.parse('2019-12-20'))
      expect(golang_event.duration).to eq(Date.parse('2019-12-21')..Date.parse('2019-12-30'))
    end
  end
end
