require 'rack/test'
require 'rspec'
require 'database_cleaner'
require 'pry'

ENV['RACK_ENV'] = 'test'

require File.expand_path '../../events_app.rb', __FILE__

module RSpecMixin
  include Rack::Test::Methods

  def app()
    EventsApp
  end
end

RSpec.configure do |config|
  config.include RSpecMixin

  config.before :each do |example|
    DatabaseCleaner.start
  end

  config.append_after :each do
    DatabaseCleaner.clean
  end
end
