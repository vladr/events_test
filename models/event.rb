class Event < ActiveRecord::Base
  attr_accessor :start_date, :end_date

  validates :title, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :duration, presence: true
  validate :duration_ok?, if: -> { duration.present? }

  before_validation :set_duration, on: [ :create ]

  def duration_ok?
    if Event.where("duration && ?", ActiveRecord::Base.connection.type_cast(duration)).present?
      errors.add(:duration, "already exists")
    end
  end

  def start_date=(value)
    @start_date = Date.parse(value) rescue nil
  end

  def end_date=(value)
    @end_date = Date.parse(value) rescue nil
  end

  private

  def set_duration
    if start_date && end_date
      self.duration = start_date..end_date
    end
  end
end
