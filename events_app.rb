require 'sinatra'
require 'sinatra/activerecord'
require 'pry'

class EventsApp < Sinatra::Base
  get '/events' do
    content_type :json
    events = Event.all
    events.to_json
  end

  post '/events' do
    content_type :json
    payload = JSON.parse(request.body.read)
    event_atts = payload.deep_symbolize_keys![:event]&.slice(:title, :start_date, :end_date) || {}
    event = Event.new(event_atts)
    if event.save
      status 201
      event.to_json
    else
      status 422
      { errors: event.errors.messages }.to_json
    end
  end

  delete '/events/:id' do
    event = Event.find(params[:id])
    event.destroy
  end

  current_dir = Dir.pwd
  Dir["#{current_dir}/models/*.rb"].each { |file| require file }
end

