class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :title
      t.daterange :duration

      t.timestamps null: false
    end
    add_index :events, :duration, using: 'gist'
  end
end
