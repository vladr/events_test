def create_events
  Event.create(title: "Ruby Conf", start_date: '2019-12-01', end_date: '2019-12-20')
  Event.create(title: "Elixir Conf", start_date: '2019-12-21', end_date: '2019-12-30')
end

create_events
